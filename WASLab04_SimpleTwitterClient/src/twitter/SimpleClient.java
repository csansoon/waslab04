package twitter;

import java.util.Date;

import twitter4j.DirectMessage;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		final Twitter twitter = new TwitterFactory().getInstance();
		
		
		TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
			
		UserStreamListener listener = new UserStreamListener() {
			@Override
            public void onStatus(Status status) {
				String nom = status.getUser().getName();
				String screen_name = status.getUser().getScreenName();
				String text = status.getText();
				
                System.out.println(nom + " (@" + screen_name + "): " + text + "\n");
            }

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {}

			@Override
			public void onScrubGeo(long arg0, long arg1) {}

			@Override
			public void onStallWarning(StallWarning arg0) {}

			@Override
			public void onTrackLimitationNotice(int arg0) {}

			@Override
			public void onException(Exception arg0) {}

			@Override
			public void onBlock(User arg0, User arg1) {}

			@Override
			public void onDeletionNotice(long arg0, long arg1) {}
			
			@Override
			public void onDirectMessage(DirectMessage arg0) {}

			@Override
			public void onFavorite(User arg0, User arg1, Status arg2){}

			@Override
			public void onFavoritedRetweet(User arg0, User arg1, Status arg2) {}

			@Override
			public void onFollow(User arg0, User arg1) {}

			@Override
			public void onFriendList(long[] arg0) {}

			@Override
			public void onQuotedTweet(User arg0, User arg1, Status arg2) {}
			
			@Override
			public void onRetweetedRetweet(User arg0, User arg1, Status arg2) {}
			
			@Override
			public void onUnblock(User arg0, User arg1) {}

			@Override
			public void onUnfavorite(User arg0, User arg1, Status arg2){}

			@Override
			public void onUnfollow(User arg0, User arg1) {}
			
			@Override
			public void onUserDeletion(long arg0) {}
			
			@Override
			public void onUserListCreation(User arg0, UserList arg1){}

			@Override
			public void onUserListDeletion(User arg0, UserList arg1) {}

			@Override
			public void onUserListMemberAddition(User arg0, User arg1, UserList arg2){} 

			@Override
			public void onUserListMemberDeletion(User arg0, User arg1, UserList arg2) {}

			@Override
			public void onUserListSubscription(User arg0, User arg1, UserList arg2) {}

			@Override
			public void onUserListUnsubscription(User arg0, User arg1, UserList arg2) {}

			@Override
			public void onUserListUpdate(User arg0, UserList arg1) {}

			@Override
			public void onUserProfileUpdate(User arg0) {}

			@Override
			public void onUserSuspension(long arg0) {}
		};
		
		
		twitterStream.addListener(listener);
		

		twitterStream.filter("#covid19");
	}
}
